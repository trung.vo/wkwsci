"""Base for WKWSCI sentiment analysis."""


from __future__ import absolute_import, division, print_function

import codecs

import re

from os.path import dirname, join


LANGUAGE_TO_FILENAME = {
    'en': 'WKWSCI-en.txt',
    'emoticons': 'WKWSCI-emoticon-8.txt',
    }


class WKWSCIException(Exception):
    """Base for exceptions raised in this module."""

    pass


class WordListReadingError(WKWSCIException):
    """Exception for error when reading information form data files."""

    pass


class WKWSCI(object):


    def __init__(self, language="en", emoticons=False, word_boundary=True):

        filename = LANGUAGE_TO_FILENAME[language]
        full_filename = self.full_filename(filename)
        if emoticons:
            # Words
            self._dict = self.read_word_file(full_filename)
            regex_words = self.regex_from_tokens(
                list(self._dict),
                word_boundary=True, capture=False)

            # Emoticons
            filename_emoticons = LANGUAGE_TO_FILENAME['emoticons']
            full_filename_emoticons = self.full_filename(filename_emoticons)
            emoticons_and_score = self.read_word_file(full_filename_emoticons)
            self._dict.update(emoticons_and_score)
            regex_emoticons = self.regex_from_tokens(
                list(emoticons_and_score), word_boundary=False,
                capture=False)

            # Combined words and emoticon regular expression
            regex = '(' + regex_words + '|' + regex_emoticons + ')'
            self._setup_pattern_from_regex(regex)

        else:
            self.setup_from_file(full_filename, word_boundary=word_boundary)

        self._word_pattern = re.compile('\w+', flags=re.UNICODE)

    def data_dir(self):

        return join(dirname(__file__), 'data')

    def full_filename(self, filename):

        return join(self.data_dir(), filename)

    def setup_from_file(self, filename, word_boundary=True):

        self._dict = self.read_word_file(filename)
        self._setup_pattern_from_dict(word_boundary=word_boundary)

    @staticmethod
    def read_word_file(filename):

        word_dict = {}
        with codecs.open(filename, encoding='UTF-8') as fid:
            for n, line in enumerate(fid):
                try:
                    word, score = line.strip().split('\t')
                except ValueError:
                    msg = 'Error in line %d of %s' % (n + 1, filename)
                    raise WordListReadingError(msg)
                word_dict[word] = int(score)
        return word_dict

    @staticmethod
    def regex_from_tokens(tokens, word_boundary=True, capture=True):

        tokens_ = tokens[:]

        # The longest tokens are first in the list
        tokens_.sort(key=lambda word: len(word), reverse=True)

        # Some tokens might contain parentheses or other problematic characters
        tokens_ = [re.escape(word) for word in tokens_]

        # Build regular expression
        regex = '(?:' + "|".join(tokens_) + ')'
        if word_boundary:
            regex = r"\b" + regex + r"\b"
        if capture:
            regex = '(' + regex + ')'

        return regex

    def _setup_pattern_from_regex(self, regex):
        self._pattern = re.compile(regex, flags=re.UNICODE)

    def _setup_pattern_from_dict(self, word_boundary=True):

        regex = self.regex_from_tokens(
            list(self._dict),
            word_boundary=word_boundary)
        self._setup_pattern_from_regex(regex)

    def find_all(self, text, clean_whitespace=True):

        if clean_whitespace:
            text = re.sub(r"\s+", " ", text)
        words = self._pattern.findall(text.lower())
        return words

    def split(self, text):

        wordlist = self._word_pattern.findall(text)
        return wordlist

    def score_with_pattern(self, text):

        word_scores = self.scores_with_pattern(text)
        score = float(sum(word_scores))
        return score

    def scores_with_pattern(self, text):

        # TODO: ":D" is not matched
        words = self.find_all(text)
        scores = [self._dict[word] for word in words]
        return scores

    def score_with_wordlist(self, text):

        words = self.split(text)
        word_scores = (self._dict.get(word.lower(), 0.0) for word in words)
        score = float(sum(word_scores))
        return score

    score = score_with_pattern

    scores = scores_with_pattern
