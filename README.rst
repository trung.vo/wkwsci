wkwsci

=====

WKWSCI sentiment analysis in Python: Lexicon-based approach for sentiment analysis.

Examples
--------

    >>> from wkwsci import wkwsci
    >>> wkwsci = WKWSCI()
    >>> wkwsci.score('This product is brilliant!')
    3.0


