"""Test of wkwsci."""


import io

import sys

from os import listdir
from os.path import join

from wkwsci import wkwsci



# https://stackoverflow.com/questions/6625782
if sys.version_info[0] == 2:
    import codecs
    u = lambda s: codecs.unicode_escape_decode(s)[0]
elif sys.version_info[0] == 3:
    u = lambda s: s


def test_wkwsci():
    wkwsci = WKWSCI()
    assert isinstance(wkwsci, WKWSCI)


def test_find_all():
    wkwsci = WKWSCI()
    words = wkwsci.find_all("It is so bad")
    assert words == ['bad']


def test_split():
    wkwsci = WKWSCI()
    words = wkwsci.split('Hello, World')
    assert words == ['Hello', 'World']

    words = wkwsci.split(u('Hell\xf8, \xc5rld'))
    assert words == [u('Hell\xf8'), u('\xc5rld')]


def test_score():
    wkwsci = WKWSCI()
    score = wkwsci.score('bad')
    assert score < 0

    score = wkwsci.score('')
    assert score == 0.0


def test_score_language():
    wkwsci = WKWSCI(language='en')
    score = wkwsci.score('bad')
    assert score < 0

    wkwsci = WKWSCI('en')
    score = wkwsci.score('bad')
    assert score < 0


def test_unicode():
    wkwsci = WKWSCI()
    score = wkwsci.score(u('na\xefve'))
    assert score < 0


def test_danish():
    wkwsci = WKWSCI(language='da')
    score = wkwsci.score('bedrageri')
    assert score < 0

    score = wkwsci.score(u('besv\xe6r'))
    assert score < 0

    score = wkwsci.score(u('D\xc5RLIG!!!'))
    assert score < 0


def test_french():
    wkwsci = WKWSCI(language='fr')
    score = wkwsci.score('accidentelle')
    assert score < 0

    score = wkwsci.score(u('accus\xe9'))
    assert score < 0

    score = wkwsci.score(u('sans charme'))
    assert score < 0


def test_swedish():
    wkwsci = WKWSCI(language='sv')
    score = wkwsci.score('befrias')
    assert score > 0

    score = wkwsci.score(u('utm\xe4rkelse'))
    assert score > 0

    score = wkwsci.score(u('ett snyggt'))
    assert score > 0


def test_score_with_pattern():
    wkwsci = WKWSCI(language='da')
    score = wkwsci.score('ikke god')
    assert score < 0

    score = wkwsci.score('ikke god.')
    assert score < 0

    score = wkwsci.score('IKKE GOD-')
    assert score < 0

    score = wkwsci.score('ikke   god')
    assert score < 0

    score = wkwsci.score('En tv-succes sidste gang.')
    assert score > 0

    score = wkwsci.score('')
    assert score == 0.0


def test_score_with_wordlist():
    wkwsci = WKWSCI()
    score = wkwsci.score_with_wordlist('Rather good.')
    assert score > 0

    score = wkwsci.score_with_wordlist('Rather GOOD.')
    assert score > 0


def test_score_with_wordlist_empty():
    wkwsci = WKWSCI()
    score = wkwsci.score_with_wordlist('')
    assert score == 0.0


def test_data():
    """Test data files for format."""
    wkwsci = WKWSCI()
    filenames = listdir(wkwsci.data_dir())
    for filename in filenames:
        if not filename.endswith('.txt'):
            continue
        full_filename = join(wkwsci.data_dir(), filename)
        with io.open(full_filename, encoding='UTF-8') as fid:
            for line in fid:
                # There should be the phrase and the score
                # and nothing more
                assert len(line.split('\t')) == 2

                # The score should be interpretable as an int
                phrase, score = line.split('\t')
                assert type(int(score)) == int


def test_emoticon():
    wkwsci = WKWSCI()
    wkwsci.setup_from_file(join(wkwsci.data_dir(), 'WKWSCI-emoticon-8.txt'),
                          word_boundary=False)
    score = wkwsci.score(':-)')
    assert score > 0

    score = wkwsci.score('This is a :-) smiley')
    assert score > 0

    score = wkwsci.score('Just so XOXO.')
    assert score > 0


def test_words_and_emoticons():
    wkwsci = WKWSCI(emoticons=True)

    score = wkwsci.score(':-)')
    assert score > 0

    score = wkwsci.score('BAD BAD BAD :-)')
    assert score < 0


def test_emoticon_upper_case():
    wkwsci = WKWSCI()
    wkwsci.setup_from_file(join(wkwsci.data_dir(), 'WKWSCI-emoticon-8.txt'),
                          word_boundary=False)

    score = wkwsci.score(':d')
    assert score == 0

    # TODO
    score = wkwsci.score(':D')
    # assert score > 0

    score = wkwsci.score('It is so: :D')
    # assert score > 0
