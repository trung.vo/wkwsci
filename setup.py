"""For packaging and installation."""

from setuptools import setup


setup(
    name='WKWSCI',
    packages=['WKWSCI'],
    version='0.1',
    author='Vo Thanh Trung',
    author_email='w170012@e.ntu.edu.sg',
    description='WKWSCI lexicon-based sentiment analysis',
    license='Apache License 2.0',
    keywords='WKWSCI lexicon-based sentiment analysis',
    url='https://gitlab.com/trung.vo/wkwsci',
    package_data={'wkwsci': ['data/*.txt', 'data/LICENSE']},
    long_description='',
    classifiers=[
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        ],
    )
