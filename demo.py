from wkwsci import wkwsci

wkw = wkwsci.WKWSCI()

print(wkw.score("this product is brilliant"))
print(wkw.score("this product is good"))
print(wkw.score("this product is normal"))
print(wkw.score("this camera model is EOS"))
print(wkw.score("this product is abnormal"))
print(wkw.score("this product is bad"))
print(wkw.score("this product is awful"))

